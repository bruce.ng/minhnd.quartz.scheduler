package com.quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class QuartzJob implements Job {
	public void execute(JobExecutionContext context) throws JobExecutionException {
		System.out.println("Java web application + Quartz 2.2.1");
	}
}

/**
 * Servlet Listener
 
	Now Create servlet listener class by implementing ServletContextListener interface
and override contextInitialized and contextDestroyed methods with your logic’s.
contextInitialized() method will be executed automatically during Servlet container initialization,
which in turn calls the Quartz scheduler job, which gets executed every 10 seconds.
contextDestroyed() method will be executed when the application shuts down, So in this function
I have invoked the shutdown function of quartz scheduler
 * */
