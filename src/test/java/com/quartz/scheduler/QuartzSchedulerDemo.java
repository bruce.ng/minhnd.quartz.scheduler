package com.quartz.scheduler;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

public class QuartzSchedulerDemo {
	public static void SimpleTriggerExample() throws SchedulerException {
		
		JobDetail job = newJob(QuartzJob.class).withIdentity("SimpleQuartzTrigger", "Group1").build();
		
		Trigger trigger = newTrigger().withIdentity("TriggerName", "Group1")
				.withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(10).repeatForever()).build();
		
		Scheduler sched = new StdSchedulerFactory().getScheduler();
		sched.scheduleJob(job, trigger);
		sched.start();
	}

	public static void CronTriggerExample() throws SchedulerException {
		JobDetail job = newJob(QuartzJob.class).withIdentity("CronQuartzTrigger", "Group2").build();
		
		Trigger trigger = newTrigger().withIdentity("TriggerName", "Group2")
				.withSchedule(CronScheduleBuilder.cronSchedule("0/5 * * * * ?")).build();
		
		Scheduler scheduler = new StdSchedulerFactory().getScheduler();
		scheduler.start();
		scheduler.scheduleJob(job, trigger);
	}
	
	public static void main(String[] args) throws SchedulerException {
		SimpleTriggerExample();
		CronTriggerExample();
	}
}
